Tableau Point Cloud Server
==================

## Prerequisites: ##
1. [Virtual Box](https://www.virtualbox.org/wiki/Downloads)
2. [Vagrant](http://www.vagrantup.com/downloads.html)

Clone repository

`git clone https://a_jackson@bitbucket.org/a_jackson/tableau-pts-server.git`

Switch to the new folder

`cd tableau-pts-server`

Initialize Vagrant Box

`vagrant up`

From local machine access database

`PGUSER=fed PGPASSWORD=fed psql -h localhost -p 15432 pointclouds`

Execute query to create tables

```
CREATE TABLE points
(
	"X" double precision,
	"Y" double precision,
	"Z" double precision,
	"N" double precision
);
```

Import cvs file (replace 'mycsv.csv' with name of file in this working folder)

```
COPY points FROM '/mnt/bootstrap/mycsv.csv' DELIMITER ',' CSV;
```

**Note:**
The expected format of the cvs file would be comma separated with (4) columns.  The first (3) columns represent X, Y, & Z coordinates making up a single point. Example:

```
-20.6822445,-15.6919225,29.3491475,74
-20.8142445,-15.6829225,29.3521475,77
-20.4612445,-15.6849225,29.3501475,72
-20.6022445,-15.7209225,29.3511475,73
-20.4902445,-15.6779225,29.3491475,73
-20.4132445,-15.6789225,29.3491475,73
-20.7202445,-15.7069225,29.3501475,78
```